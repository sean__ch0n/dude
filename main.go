package main

import (
	"fmt"
	"log"
	"net"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)

var (
	pcapFile string = "capt.pcap"
	handle   *pcap.Handle
	err      error
)

func main() {
	// open file
	handle, err = pcap.OpenOffline(pcapFile)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	hosts := printPacketInfo(packetSource)
	for k, v := range hosts {
		fmt.Println("Host", k)
		fmt.Println("==================")
		fmt.Println("ip: ", v.ipAddr)
		for _, conV := range v.connections {
			fmt.Println("talked to: ")
			fmt.Println(conV.ipAddr, ":", conV.macAddr, conV.proto)
		}
		fmt.Println("---")
	}
}

type connectionInfo struct {
	ipAddr  net.IP
	macAddr net.HardwareAddr
	proto   layers.IPProtocol
}

type host struct {
	ipAddr      net.IP
	connections []connectionInfo
}

func printPacketInfo(packetSource *gopacket.PacketSource) map[*net.HardwareAddr]host {
	// if ethernet packet parse info
	hosts := map[*net.HardwareAddr]host{}
	for packet := range packetSource.Packets() {
		ethernetLayer := packet.Layer(layers.LayerTypeEthernet)
		var srcMac net.HardwareAddr
		var dstMac net.HardwareAddr
		var srcIP net.IP
		var dstIP net.IP
		var proto layers.IPProtocol

		if ethernetLayer != nil {
			fmt.Println("found ethernet")
			ethernetPacket, _ := ethernetLayer.(*layers.Ethernet)
			srcMac = ethernetPacket.SrcMAC
			dstMac = ethernetPacket.DstMAC
			fmt.Println("source mac: ", ethernetPacket.SrcMAC)
			fmt.Println("dest mac", ethernetPacket.DstMAC)
			fmt.Println()
		}

		// if IP packet
		ipLayer := packet.Layer(layers.LayerTypeIPv4)
		if ipLayer != nil {
			fmt.Println("ipv4 found")
			ip, _ := ipLayer.(*layers.IPv4)
			fmt.Println(ip.SrcIP, " -> ", ip.DstIP)
			fmt.Println("over ", ip.Protocol)
			srcIP = ip.SrcIP
			dstIP = ip.DstIP
			proto = ip.Protocol
			fmt.Println()
		}
		if existingHost, exists := hosts[&srcMac]; exists {
			// this host exists so lets append to host.connections
			existingHost.connections = append(existingHost.connections,
				connectionInfo{ipAddr: dstIP, macAddr: dstMac})
		} else {
			// this is a new host, add to hosts
			c := make([]connectionInfo, 0)
			c = append(c, connectionInfo{ipAddr: dstIP, macAddr: dstMac, proto: proto})
			hosts[&srcMac] = host{
				ipAddr:      srcIP,
				connections: c,
			}
		}

	}

	return hosts
}
